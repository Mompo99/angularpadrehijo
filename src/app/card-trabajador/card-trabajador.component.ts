import { Component, OnInit } from '@angular/core';

// importar Input, Output y Event Emitter from '@angular/core'

import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

// importar Trabajador
import { Trabajador } from '../Modelos/trabajador';
 

@Component({
  selector: 'app-card-trabajador',
  templateUrl: './card-trabajador.component.html',
  styleUrls: ['./card-trabajador.component.css']
})
export class CardTrabajadorComponent implements OnInit {
  
  // decorador input para el trabajador que se le pasa desde el padre
  @Input() trabajador: Trabajador;
  
  // decorador output para el evento likeTrabajador 
  @Output() likeTrabajador = new EventEmitter<number>();

  // decorador output para el evento unlikeTrabajador
  @Output() unlikeTrabajador = new EventEmitter<number>();

  // decorador output para el evento borraTrabajador
  @Output() borraTrabajador = new EventEmitter<number>();
    
  constructor( ) {
    
  }

  ngOnInit(): void {
  }

  //metodo que recibe un trabajador y emite el evento likeTrabajador con el id del trabajador
  like(trabajadores:Trabajador){
    this.likeTrabajador.emit(trabajadores.id);
  }

  //metodo que recibe un trabajador y emite el evento unlikeTrabajador con el id del trabajador
  unlike(trabajadores:Trabajador){
    this.unlikeTrabajador.emit(trabajadores.id);
  }

  //metodo que recibe un trabajador y emite el evento borraTrabajador con el id del trabajador
  eliminar(trabajadores:Trabajador){
    this.borraTrabajador.emit(trabajadores.id);
  }
}