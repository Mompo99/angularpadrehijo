import { Component } from '@angular/core';

//importar el interface trabajador
import { Trabajador } from './Modelos/trabajador';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  //declarar la variable titulo

  title: String = "Práctica Padre-hijo";
  
 //declarar la variable trabajadores array del tipo Trabajador

  public trabajadores:Trabajador[] = [

    {
      id:1,
      nombre: 'Ana',
      cargo: 'Programadora' ,
      foto:'1.jpg',
      votos:0
    },
     
    {
      id:2,
      nombre: 'Elena',
      cargo: 'Administrativa',
      foto:'2.jpg',
      votos:0
    },
    {
      id:3,
      nombre: 'Juan',
      cargo: 'Analista' ,
      foto:'3.jpg',
      votos:0
    },
    {
      id:4,
      nombre: 'Luis',
      cargo: 'Programador' ,
      foto:'4.jpg',
      votos:0
    },
    {
      id:5,
      nombre: 'Maria',
      cargo: 'Diseñadora' ,
      foto:'5.jpg',
      votos:0
    },
    {
      id:6,
      nombre: 'Pedro',
      cargo: 'Marketing' ,
      foto:'6.jpg',
      votos:0
    }


  ]


 // metodo para borrar el trabajador pasado por id del array de trabajadores
 borrar(id:number){
   let posicion = this.trabajadores.findIndex(trabajador=>id == trabajador.id);
   this.trabajadores.splice(posicion,1);
 }

 // metodo para sumar un voto al trabajador pasado por id
 sumaVoto(id:number){

  let posicion = this.trabajadores.findIndex(trabajador=>id == trabajador.id);
  let trabajador = this.trabajadores[posicion];
  trabajador.votos++;
 }

 // metodo para restar un voto al trabajador pasado por id
 restaVoto(id:number){

  let posicion = this.trabajadores.findIndex(trabajador=>id == trabajador.id);
  let trabajador = this.trabajadores[posicion];

  if(trabajador.votos <=0){
    alert("No se pueden restar votos");
  }else{
    trabajador.votos--;
  }

 }


}
