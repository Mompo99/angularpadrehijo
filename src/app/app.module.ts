import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Componentes

import { AppComponent } from './app.component';
import { CardTrabajadorComponent } from './card-trabajador/card-trabajador.component';

// Rutas

// Servicios




@NgModule({
  declarations: [
    AppComponent,
    CardTrabajadorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
